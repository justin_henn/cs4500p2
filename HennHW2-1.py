#Justin Henn
#9/3/2018
#CS4500-E01
#This program is a simulation of a pyramid of numbers from 1-21 and randomly moving to each number
#Python 3.6 used with Spyder and IPython Console
#Sources:
#1. https://stackoverflow.com/questions/850795/different-ways-of-clearing-lists
#2. https://pyformat.info/
#3. https://stackoverflow.com/questions/3996904/generate-random-integers-between-0-and-9

#!/usr/bin/python

from random import randint

#This function is used to pick a random movement. 0 = up right, 1 = up left,
#2 = down left, and 3 = down right. The function does not take any parameters.
#The function does return and random integer between 0 and 3. The source
#used for this was number 3.

def random_dice():
        return randint(0, 3)

#This function is used to print information about the game to the screen and to the file.
#The function takes the output file as a parameter and does not return anything.

def print_game_info(outfile):
    print("This is a game where a pyramid of numbers 1-21 has been created.")
    print("The game starts a dot at the number one then randomly will move through")
    print("the pyramid going up left, up right, down left, or down right.")
    print("If the random movement takes the dot to an invalid place, the")
    print("dot will stay where it currently is. The game ends when the dot")
    print("has been to every number in the pyramid. The game will display")
    print("every movement the dot took on the screen and to the file. After")
    print("the dot has visited every place, the game will then print the total")
    print("number of movements and the average for each number. It will also")
    print("print which number/numbers were visited the most. \n")
    outfile.write("This is a game where a pyramid of numbers 1-21 has been created. ")
    outfile.write("The game starts a dot at the number one then randomly will move through ")
    outfile.write("the pyramid going up left, up right, down left, or down right. ")
    outfile.write("If the random movement takes the dot to an invalid place, the ")
    outfile.write("dot will stay where it currently is. The game ends when the dot ")
    outfile.write("has been to every number in the pyramid. The game will display ")
    outfile.write("every movement the dot took on the screen and to the file. After ")
    outfile.write("the dot has visited every place, the game will then print the total ")
    outfile.write("number of movements and the average for each number. It will also ")
    outfile.write("print which number/numbers were visited the most. \n\n")

#This function is used to create the pyramid in a list of lists. It first
#initiates counter z and g. It then check to see if g is less than the the total
#amount of numbers that are supposed to be in the pyramid. For this example
#that is 21. It then initializes x to 0 and the list b to empty. It then checks
# to see if x is less than z. This is used to figure out how many numbers
# are in each list. Since each pyramid row adds one more number than the former
#pyramid row, this can be used to know when a list has the correct amount of
#numbers in it. It creates this "pyramid row list" in the list b. After b
#has the correct amount of numbers it then updates z so the loop knows to add
#another number to the new row it creates. Finally it appends the list b
#to full_list. This full_list is a recreation of the pyramid in a list of lists
#The empty full_list is passed to the function and the total amount of the numbers
#in the pyramid which is represented by total_numbers. The function does not
#return anything. The source used for deleting an array is number 1.

def create_list(full_list, total_numbers):
        #i = 0
        z = 1
        g = 1

        while g < total_numbers:
                x = 0
                b = []
                while x < z:
                        b.append(g)
                        g += 1
                        x += 1
                z += 1
                full_list.append(b)
                del(b)
                
#This function is used to print the totals of the numbers. It first creates
#a highest_numbers list which is used to keep track of the numbers that got
#visited the most. It then creates the highest_total variable which keeps track
#of what highest number for the number visited. It the prints total number of 
#of moves. After that it prints the number itself and the average times it was
#visited in a table like form. The loop as checks the total for each number
#and sees if that number has the highest amount of visits. If it does, then
#it updates highest_total with that number and puts it in the highest_numbers
#list. If it finds another number that has a higher amount of visits then it 
#deletes everything in the highest_numbers list and adds that new number instead.
#It also updates highest_total. If a number's visits is equal to the highest_total
#then it will append that number to the highest_numbers list. It will then
#print the highest numbers. The parameters passed to the function are the output
#file and the dot_total list that keeps track of the amount of times a number
#has been visited. The function does not return anything. The source used for 
#formatting is number 2.

def print_totals(outfile, dot_total):
        highest_numbers = [0]
        highest_total = 0
        print("Total number of moves: %d\n" % sum(dot_total))
        print("Info for numbers:")
        print("Number%12s" % "Average")
        outfile.write("Total number of moves: %d\n\n" % sum(dot_total))
        outfile.write("Info for numbers:\n")
        outfile.write("Number%12s\n" % "Average")
        for i in range(0, len(dot_total)):
                print("%3d%15.4f" % ((i+1), (float(dot_total[i])/float(sum(dot_total)))))
                outfile.write("%3d%15.4f\n" % ((i+1), (float(dot_total[i])/float(sum(dot_total)))))
                if dot_total[i] > highest_total:
                        #for x in range(1, len(highest_numbers)):
                        #        del highest_numbers[x]
                            del highest_numbers[:]
                            highest_numbers.append(i+1)
                            highest_total = dot_total[i]
                elif dot_total[i] == highest_total:
                        highest_numbers.append(i+1)
                else:
                    continue

        print("Largest number of dots:", end='')
        print(highest_total)
        outfile.write("Largest number of dots:")
        outfile.write(str(highest_total))

#This function is used to print the place where the dot is located. It first
#prints where it is, then it checks to see if it should print a comma or a
#period after. THe period is supposed to be printed if the dot has visited
#every spot, so that is why it checks to see if every dot_total is something
#other than 0. If it is, then that number has been visited. The function takes
#the output file, the full_list pyramid list, the x_coord and y_coord 
#variables used to access the full_list, the dot_total list of times a number
#was visited, and the total_numbers which is the number count in the pyramid.
#The function does not return anything.

def print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers):

        print(full_list[x_coord][y_coord], end='')
        outfile.write("%d" % full_list[x_coord][y_coord])
        print_comma = 0
        for i in range(0, total_numbers):
                if dot_total[i] == 0:
                        print_comma = 1
                        break
        if print_comma == 0:
                print(".\n")
                outfile.write(".\n\n")
        else:
                print(", ", end='')
                outfile.write(", ")

#This funciton is used for the pyramid traversal. It first creates dot_total
#which is a list to keep the total amount of times a number has been visited
#per number. done_checker is used to see if every dot has been visited. The
#first dot_total element is then set to 1 as when the game starts the dot is
#moved to one. The x_coord and y_coord variables are used to traverse the lists
#Right now they are initialized to 0,0 because we are at the number 1 which is
#in the first list, first element, of the full_list. The x_coord shows what list
#we are in, and the y_coord shows what element we are on. It then prints that info.
#Next it checks to see if done_checker is 0. If not then it loops through the
#the dot_total list to see if everything has been visited. If not it will keep
#done_checker set to 0. It then checks to see if done_checker is 0 and if not
#it rolls a random number using the function. It then checks to see if that
#number is 0. If it is then we know we are going up right. THat means we will
#be subtracting one from the x_coord and from the y_coord, since we are going 
#up one list, and back one element. We know if both the x_coord_and y_coord will
#be less than 0 if we do the subtraction that means we cannot move. The if statement
#checks that if the subtraction will be an issue then it just prints the current
#spot we are at since we couldn't move. Else, we update the coords and print where
#we went. Either way we also update the visit totals. If the random number is
#1 then that means up right. That means we subtract 1 from x_coord and we add one
#to y_coord. We check in the if statement to see if minus 1 from the x_coord will
#be less than 0 and if the y_coord is already set to the last element of the list
#we will be going to. If that is so then we can't move and just print where we
#are at. If it okay, then we will subtract one from x_coord and print our new
#location. It will also updates the visit totals as well. It then checks to see
#if the random number is 2. That would mean to move down left. That means moving
#down one list and to the left. It checks to see if x_coord gets updated by one
#if that will go past the amount of lists in the full_list. If that happens then
#the dot stays where it's at. If not then it updates x_coord and prints the new 
#number. It also updates the visit totals. Finally, if the random number is 3
#that means the dot moves down and right. It first checks to see if adding one
#to the x_coord will be out of bounds for the number of lists. If not, then it
#it checks to see if y_coord will be beyond the number of elements in the list
#it is moving to. If either condition is found the it stays where it is at.
#If not then it will move and priunt the new number. It will update visit totals.
#This loop is done until every number has been visited. It then prints the totals.
#The function takes the output file, the full_list pyramid, and the total_numbers
#which represents the number count in the pyramid. The function does not return
#anything.

def pyramid(outfile, full_list, total_numbers):
        dot_total = [0] * total_numbers
        done_checker = 0
        dot_total[0] = 1
        x_coord = 0
        y_coord = 0
        print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
        while done_checker == 0:
            for i in range(0, total_numbers):
                if dot_total[i] == 0:
                    done_checker = 0
                    break
                else:
                    done_checker = 1
            if done_checker == 0:
                random_num = random_dice()
                if random_num == 0:
                        if (x_coord - 1) < 0 or (y_coord - 1) < 0:
                                dot_total[full_list[x_coord][y_coord]- 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                        else:
                                x_coord = x_coord - 1
                                y_coord = y_coord - 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                elif random_num == 1:
                        if (x_coord - 1) < 0 or y_coord > (len(full_list[x_coord-1]) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                        else:
                                x_coord = x_coord - 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                elif random_num == 2:
                        if (x_coord + 1) > (len(full_list) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                        else:
                                x_coord = x_coord + 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
                else:
                        if (x_coord + 1) > (len(full_list) - 1) or (y_coord + 1) > (len(full_list[x_coord+1]) - 1):
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)

                        else:
                                x_coord = x_coord + 1
                                y_coord = y_coord + 1
                                dot_total[full_list[x_coord][y_coord] - 1] += 1
                                print_place(outfile, full_list, x_coord, y_coord, dot_total, total_numbers)
        print_totals(outfile, dot_total)

#This is the "main portion of the program. It firsts opens the output file.
#It then creates a blank full_list, and sets the total amount of numbers to 21.
#It calls the create_list function and passes the 2 arguments to create the
#pyramid. It then prints the game information using print_game_info. It next
#calls the pyramid function sending the outpfile, the full_list "pyramid",
#and the total_numbers which are how many numbers are in the pyramid.
#After that it closes the file ending the program.

initoutfile = open("HW2hennOutfile.txt", 'w')
full_list = []
total_numbers = 21
create_list(full_list, total_numbers)
print_game_info(initoutfile)
pyramid(initoutfile, full_list, total_numbers)
initoutfile.close()